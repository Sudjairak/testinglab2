const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', ()=>{
    context('Function isUserNameValid', ()=> {
        it('Function prototype : boolean isUserNameValid(username: String)', ()=> {
            expect(validate.isUserNameValid('kob')).to.be.true;
        });

        it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', ()=> {
            expect(validate.isUserNameValid('tu')).to.be.false;
        });

        it('ทุกตัวต้องเป็นตัวเล็ก', ()=> {
            expect(validate.isUserNameValid('Kob')).to.be.false;
            expect(validate.isUserNameValid('KoB')).to.be.false;
        });

        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', ()=> {
            expect(validate.isUserNameValid('kob123456789012')).to.be.true;
            expect(validate.isUserNameValid('kob1234567890123')).to.be.false;
        });
    });

    context('Function isAgeValid', ()=> {
        it('Function prototype : boolean isAgeValid (age: String)', ()=> {
            expect(validate.isAgeValid('18')).to.be.true;
        });
        it('age ต้องเป็นข้อความที่เป็นตัวเลข', ()=> {
            expect(validate.isAgeValid('a')).to.be.false;
        });
        it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', ()=> {
            expect(validate.isAgeValid('17')).to.be.false;
            expect(validate.isAgeValid('18')).to.be.true;
            expect(validate.isAgeValid('100')).to.be.true;
            expect(validate.isAgeValid('101')).to.be.false;
        });
    });

    context('Function isPasswordValid', ()=> {
        it('Function prototype : boolean isPasswordValid (password: String)', ()=> {
            expect(validate.isPasswordValid('Eie?1234')).to.be.true;
        });
        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', ()=> {
            expect(validate.isPasswordValid('eie?')).to.be.false;
            expect(validate.isPasswordValid('eiE?1234')).to.be.true;
        });
        it('ต้องมีตัวอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', ()=> {
            expect(validate.isPasswordValid('eie?1234')).to.be.false;
            expect(validate.isPasswordValid('Eie?1234')).to.be.true;
            expect(validate.isPasswordValid('ei?I1234')).to.be.true;
        });
        it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', ()=> {
            expect(validate.isPasswordValid('Eie?ei34')).to.be.false;
            expect(validate.isPasswordValid('Eie?e234')).to.be.true;
            expect(validate.isPasswordValid('ei?I1234')).to.be.true;
        });
        it('ต้องมีอักขระพิเศษเป็นส่วนประกอบอย่างน้อย 1 ตัว', ()=> {
            expect(validate.isPasswordValid('Eie?ei34')).to.be.false;
            expect(validate.isPasswordValid('Eie?e234')).to.be.true;
            expect(validate.isPasswordValid('ei?I1234')).to.be.true;
        });
    });

    context('Function isDateValid', ()=> {
        it('Function prototype : boolean isDateValid (day : Integer, month : Integer, year: Integer)', ()=> {
            expect(validate.isDateValid(12,12,2012)).to.be.true;
        });
        it('day เริ่ม 1 และไม่เกิน 31 ในทุกๆ เดือน', ()=> {
            expect(validate.isDateValid(12,12,2012)).to.be.true;
            expect(validate.isDateValid(32,12,2012)).to.be.false;
        });
        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุกๆ ปี', ()=> {
            expect(validate.isDateValid(12,12,2012)).to.be.true;
            expect(validate.isDateValid(12,13,2012)).to.be.false;
        });
        it('year จะต้องไม่ต่ำกว่าปี 1970 และไม่เกิน 2020', ()=> {
            expect(validate.isDateValid(12,12,2020)).to.be.true;
            expect(validate.isDateValid(12,12,2021)).to.be.false;
        });
        it('เดือนแต่ละเดือนมีจำนวนวันต่างกัน', ()=> {
            expect(validate.isDateValid(31,1,2001)).to.be.true;
            expect(validate.isDateValid(31,2,2000)).to.be.false;
        });
        it('year ที่เป็นปีอธิกสุรทิน จะต้องหาร 400 ลงตัว', ()=> {
            expect(validate.isDateValid(29,2,2001)).to.be.false;
            expect(validate.isDateValid(29,2,2000)).to.be.true;
        });
    });


});