// const isUserNameValid = function(username) {
//     return true;
// }

module.exports = {
    isUserNameValid: function (username) {
        if (username.length < 3 || username.length > 15) {
            return false;
        }
        // Kob -> kob
        if (username.toLowerCase() !== username) {
            return false;
        }
        return true;
    },

    isAgeValid: function (age) {
        const regex = /^[0-9]*$/
        if (!regex.test(age)) {
            return false;
        }
        if (age < 18 || age > 100) {
            return false;
        }
        return true;
    },

    isPasswordValid: function (password) {
        if (password.length < 8) {
            return false;
        }
        const atLeastOneUpper = /(?=.*[A-Z])/
        if (!atLeastOneUpper.test(password)) {
            return false;
        }
        const atLeastThreeDigits = /^(?:\D*\d){3}/
        if (!atLeastThreeDigits.test(password)) {
            return false;
        }
        const atLeastSpecial = /[\W_]/
        //[!@#$%^&*()_+|~-=\`{}[]:";'<>?,./]
        if (!atLeastSpecial.test(password)) {
            return false;
        }
        return true;
    },

    isDateValid: function (day, month, year) {

        if (year < 1970 || year > 2020) {
            return false;
        }
        if (month < 1 || month > 12) {
            return false;
        }
        switch (month) {
            case 1, 3, 5, 7, 8, 10, 12:
                if (day < 1 || day > 31)
                    return false;
            case 2:
                if (year % 400 != 0) {
                    if (day < 1 || day > 28)
                        return false;
                }
                if (year % 400 == 0) {
                    if (day < 1 || day > 29)
                        return false;
                }
            case 4, 6, 9, 11:
                if (day < 1 || day > 30)
                    return false;
        }
        return true;
    }

}